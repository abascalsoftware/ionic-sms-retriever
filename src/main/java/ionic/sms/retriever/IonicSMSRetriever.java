package ionic.sms.retriever;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.auth.api.phone.SmsRetrieverClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import java.lang.Math;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;
import org.apache.cordova.PluginResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * This class echoes a string called from JavaScript.
 */
public class IonicSMSRetriever extends CordovaPlugin implements SMSReceiver.OTPReceiveListener {
	private CallbackContext callback;
	public static final int INTENT_START = 1;
	public static final int INTENT_STOP = 2;
	private SMSReceiver smsReceiver;
	private String TAG = "IonicSMSRetriever";

	@Override
	public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
		callback = callbackContext;
		Log.d(TAG, "IonicSMSRetriever execute");
		Log.d(TAG, "IonicSMSRetriever action: " + action.toString());
		Log.d(TAG, "IonicSMSRetriever args: " + args.toString());
		if (action.equals("teste")) {
			int num = args.getInt(0);
			String message = args.getString(1);
			JSONObject json = new JSONObject();
			json.put("num", Math.pow(new Double(num), 2));
			json.put("message", "CHEGOU NO CORDOVA E VOLTOU!");
			this.teste(json, callbackContext);
			return true;
		}else if(action.equals("start")){
			/*Log.d(TAG, "execute action start 00");
			try {
				Log.d(TAG, "execute action start 01");
				Context context = cordova.getActivity().getApplicationContext();
				Log.d(TAG, "execute action start 02");
				Intent intent = new Intent(context, SMSActivity.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				Log.d(TAG, "execute action start 03");
				intent.putExtra("action", IonicSMSRetriever.INTENT_START);
				Log.d(TAG, "execute action start 04");
				cordova.startActivityForResult((CordovaPlugin) this, intent, IonicSMSRetriever.INTENT_START);
				Log.d(TAG, "execute action start 05");
				PluginResult result = new PluginResult(PluginResult.Status.NO_RESULT);
				Log.d(TAG, "execute action start 06");
				result.setKeepCallback(true);
				Log.d(TAG, "execute action start 07");
				callback.sendPluginResult(result);
				Log.d(TAG, "execute action start 08");
			}catch(Exception e){
				e.printStackTrace();
			}*/
			AppSignatureHashHelper appSignatureHashHelper = new AppSignatureHashHelper(webView.getContext());
			Log.i(TAG, "HashKey: " + appSignatureHashHelper.getAppSignatures().get(0));
			startSMSListener();
			return true;
		}else if(action.equals("stop")){

		}
		return false;
	}

	@Override
    public void initialize(CordovaInterface cordova, CordovaWebView webView) {
        super.initialize(cordova, webView);
    }

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data){
		if(data != null){
			Log.d(TAG,"onActivityResult... 1");
			Bundle extras = data.getExtras();
			Log.d(TAG,"onActivityResult... 2");
			for (String key : extras.keySet()) {
				Log.d(TAG,"onActivityResult... xyz");
				Object value = extras.get(key);
				Log.d(TAG, "onActivityResult Bundle extras -> "+String.format("%s %s (%s)", key,value.toString(), value.getClass().getName()));
			}
			Log.d(TAG,"onActivityResult 3");
		}
		switch(requestCode) {
			case IonicSMSRetriever.INTENT_START:
			Log.d(TAG, "onActivityResult START 01");
				Bundle extras = new Bundle();
				Log.d(TAG, "onActivityResult START 02");
				if(data != null){
					Log.d(TAG, "onActivityResult START 02.1");
					extras = data.getExtras();
				}
				Log.d(TAG, "onActivityResult START 03");
				if(resultCode == cordova.getActivity().RESULT_OK) {
					Log.d(TAG, "onActivityResult START 03.1");
					callback.sendPluginResult(new PluginResult(PluginResult.Status.OK, extras.getString("data")));
					Log.d(TAG, "onActivityResult START 04.1");
				}else if(resultCode == 0){
					Log.d(TAG, "onActivityResult START 03.2");
					callback.sendPluginResult(new PluginResult(PluginResult.Status.NO_RESULT,""));
					Log.d(TAG, "onActivityResult START 04.2");
				}else{
					Log.d(TAG, "onActivityResult START 03.3");
					String error = "Erro desconhecido";
					Log.d(TAG, "onActivityResult START 04.3");
					if(extras.containsKey("data") && !extras.getString("data").equals(null)){
						Log.d(TAG, "onActivityResult START 04.3.1");
						error = extras.getString("data");
						Log.d(TAG, "onActivityResult START 04.3.2");
					}
					Log.d(TAG, "onActivityResult START 05.3");
					callback.sendPluginResult(new PluginResult(PluginResult.Status.ERROR, error));
					Log.d(TAG, "onActivityResult START 06.3");
				}
				break;
			case IonicSMSRetriever.INTENT_STOP:
				Log.d(TAG, "onActivityResult STOP 01");
				break;
		}
	}

	/**
     * Starts SmsRetriever, which waits for ONE matching SMS message until timeout
     * (5 minutes). The matching SMS message will be sent via a Broadcast Intent with
     * action SmsRetriever#SMS_RETRIEVED_ACTION.
     */
    private void startSMSListener() {
        try {
            smsReceiver = new SMSReceiver();
            smsReceiver.setOTPListener(this);

            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(SmsRetriever.SMS_RETRIEVED_ACTION);
            webView.getContext().registerReceiver(smsReceiver, intentFilter);

            SmsRetrieverClient client = SmsRetriever.getClient(webView.getContext());

            Task<Void> task = client.startSmsRetriever();
            task.addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    // API successfully started
                }
            });

            task.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    // Fail to start API
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onOTPReceived(String otp) {
		// showToast("OTP Received: " + otp);
		Log.d(TAG,"OTP Received: "+otp);

        if (smsReceiver != null) {
            webView.getContext().unregisterReceiver(smsReceiver);
            smsReceiver = null;
		}
		callback.sendPluginResult(new PluginResult(PluginResult.Status.OK, otp));
    }

    @Override
    public void onOTPTimeOut() {
		// showToast("OTP Time out");
		Log.d(TAG,"OTP Time out");
		// sendActivityResult(AppCompatActivity.RESULT_CANCELED, "TIMEOUT");
		// sendActivityResult(Activity.RESULT_CANCELED, "TIMEOUT");
		callback.sendPluginResult(new PluginResult(PluginResult.Status.NO_RESULT,"TIMEOUT"));
    }

    @Override
    public void onOTPReceivedError(String error) {
		// showToast(error);
		Log.d(TAG,"OTP ERROR: "+error);
    }


    @Override
    public void onDestroy() {
		if (smsReceiver != null) {
            webView.getContext().unregisterReceiver(smsReceiver);
        }
        super.onDestroy();
	}

	private void teste(JSONObject res, CallbackContext callbackContext) {
		Log.d(TAG, "Cheguei no teste");
		try {
			int num = res.getInt("num");
			String message = res.getString("message");
			Log.d(TAG, "num: " + Integer.toString(num));
			Log.d(TAG, "message: " + message);
			callbackContext.success(res);
		} catch (Exception e) {
			Log.d(TAG, e.toString());
			callbackContext.error(e.toString());
		}
	}

}
